import React from "react";
import "./App.css";

class Text extends React.Component {
  render() {
    let imgUrl =
      "https://miro.medium.com/max/6000/1*djvQ0KGEH4ZJ-PwQ9oGr7g.png";
    const divStyle = {
      color: "" + this.props.color,
      backgroundImage: "url(" + imgUrl + ")",
    };
    return <div style={divStyle}>{this.props.value}</div>;
  }
}

class App extends React.Component {
  render() {
    return (
      <h1>
        Hello, <Text color="red" value="Klebson" />
      </h1>
    );
  }
}

export default App;
